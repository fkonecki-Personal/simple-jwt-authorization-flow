const express = require('express');
const authController = require('../controller/auth');

const router = express.Router();

router.post('/login', authController.requestLogin);
router.post('/token', authController.requestToken);
router.post('/data', authController.requestData);

module.exports = router;