var crypto = require('crypto');

/**
 At registration the client application is assigned a client ID and 
 a client secret (password) by the authorization server. 
 The client ID and secret is unique to the client application on that authorization server.
 **/
var clientID = "892";
var clientSecret = "2c7cbeec2c8f6f37377b4d1fa256f47e0a26b0fd614e7606fd73a6db3b071313afda05e735280c5641b1abe405e85ce129d40cb98c6246b25ef6933674540793b3c0cedb7ee4d8c5f2b7c1fcb43e012e2c19b7f0b4ec6b912ada6d3cd27edf558d66c87b311c67ce6c248121b6103fb29b09982c5cdb807ab80b5189660837f0a92a2bbd92bb7b8037c5d4d67cc1220243e9a7cf00deb8371e7ddaf09a65eaa2";

var username = "admin";
var password = "root";

var expirationTime = "3599";
var authorizationCode = "AaI5Or3RYB2uOgiyqVsLs1ATIY0ll0";

/**
 Secret is an symmetric key that is known by both the sender and the receiver. 
 It is negotiated and distributed out of band.
 **/
var secret = "0FF8AB7E6BB312573E09905D57AD9D90208B24A89BAFF59945A859D92100FA33";

var accessToken;

/**
 The ID token used to retrieve user�s basic profile information like name, DOB, email, 
 phone, which is present in authentication server. ID token should not be used to 
 gain access to an API. Both accessToken and IdToken are JWTs.
 **/
var IdToken;

function signKey(message) {
    return crypto.createHmac('sha256', secret).update(message).digest('base64');
}

//encoded = btoa(JSON.stringify(obj))
//actual = JSON.parse(atob(encoded))
function generateSignature(header, payload) {
    header = btoa(JSON.stringify(header));
    payload = btoa(JSON.stringify(payload));
    var signaturePayload = header + "." + payload;

    return btoa(JSON.stringify(header)) + "." +
        btoa(JSON.stringify(payload)) + "." +
        signKey(signaturePayload);
}

function validateToken(jwt) {
    var header = jwt["header"];
    var payload = jwt["payload"];
    var signature = jwt["signature"];

    return generateSignature(header, payload) === signature;
}

module.exports.requestLogin = ((req, res, next) => {
    if (req.body["username"] === username &&
        req.body["password"] === password)

        res.json({
            "result": "OK",
            "authorizationCode": authorizationCode
        });
    else
        res.json({ "result": "ERROR" });

})

module.exports.requestToken = ((req, res, next) => {
    if (req.body["authorizationCode"] === authorizationCode &&
        req.body["clientID"] === clientID &&
        req.body["clientSecret"] === clientSecret) {

        var header = { alg: "HS256", type: "JWT" };
        var payload = { username: username, expiresAt: expirationTime };
        accessToken = {
            header: header,
            payload: payload,
            signature: generateSignature(header, payload)
        };

        res.json({
            "result": "OK", 
            "token_type": "Bearer",
            "expires_in": expirationTime,
            "access_token": accessToken,
            "scope": "openid profile email photo",
            "id_token": IdToken
        })
    } else
        res.json({ "result": "ERROR" });
})

module.exports.requestData = ((req, res, next) => {
    if (validateToken(req.body["access_token"]))
        res.json({
            "result": "OK",
            "data": "Requested data"
        });
    else
        res.json({
            "result": "ERROR",
            "message": "Received invalid access token: " + req.body["access_token"]
        });

})