# Simple JWT authorization flow

Simplified implementation of authorization flow with JWT, as described in OAuth

## Getting started

This simplified authorization flow was implemented using NodeJS and React out of interest and to research the authorization process of OAuth.

<img src="/Screenshots/Main.png" alt="Screenshot" width="800" height="130">

Server is started by running command "npm run dev" from /Server folder.

Client is started by running command "npm start" from /Client/client folder.

## References

List of web references

1. The OAuth 2.0 Authorization Framework: https://www.rfc-editor.org/rfc/rfc6749

2. Oracle Fusion Middleware API Gateway OAuth 2.0 Authentication Flows: https://docs.oracle.com/cd/E50612_01/doc.11122/oauth_guide/content/oauth_flows.html

3. Which OAuth 2.0 Flow Should I Use?: https://auth0.com/docs/get-started/authentication-and-authorization-flow/which-oauth-2-0-flow-should-i-use

4. Client a web app executing on the server authorization code flow: https://auth0.com/docs/get-started/authentication-and-authorization-flow/authorization-code-flow

5. Different token types: https://www.c-sharpcorner.com/article/accesstoken-vs-id-token-vs-refresh-token-what-whywhen/



